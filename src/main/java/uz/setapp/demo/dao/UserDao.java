package uz.setapp.demo.dao;

import uz.setapp.demo.entity.User;

public interface UserDao extends AbstractDao<User> {
    User findByUser(String userName);
}
