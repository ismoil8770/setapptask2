package uz.setapp.demo.dao.impl;

import org.springframework.stereotype.Repository;
import uz.setapp.demo.dao.UserDao;
import uz.setapp.demo.entity.User;

import javax.persistence.Query;
import javax.persistence.metamodel.EntityType;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User> implements UserDao {


    /**
     * Instantiates a new Abstract dao.
     *
     */
    public UserDaoImpl() {
        super(User.class);
    }

    /**
     * Instantiates a new Abstract dao.
     *
     */


    @Override
    public User findByUser(String userName) {
        Query query = entityManager.createQuery("select u from User u  where u.username = :username ");
        query.setParameter("username", userName);

        return (User) query.getSingleResult();
    }
}
