package uz.setapp.demo.dao.impl;



import org.springframework.beans.factory.annotation.Autowired;
import uz.setapp.demo.dao.AbstractDao;
import uz.setapp.demo.dao.Deletable;
import uz.setapp.demo.entity.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.transaction.Transactional;
import java.util.List;

/**
 * The type Abstract dao.
 *
 * @param <T> the type parameter
 */
@SuppressWarnings("unchecked")
@Transactional
public abstract class AbstractDaoImpl<T extends BaseEntity> implements AbstractDao<T> {

    /**
     * The Entity manager.
     */
    @PersistenceContext
    public EntityManager entityManager;

    /**
     * The Entity class.
     */
    protected Class<T> entityClass;


    /**
     * Instantiates a new Abstract dao.
     *
     * @param entityClass the entity class
     */
    public AbstractDaoImpl(Class<T> entityClass) {
        this.entityClass = entityClass;

    }

    public void create(T entity) {
        this.entityManager.persist(entity);
    }

    /**
     * Creates entity if not exists, updates if entity exists in database
     * @param entity
     */
    public long save(T entity) {
        if (!(entity instanceof BaseEntity)) {
            throw new IllegalArgumentException("Passed argument is not of BaseEntity type");
        }
        BaseEntity baseEntity = ((BaseEntity) entity);
        if (baseEntity.getId() == null) {
            entityManager.persist(entity);
        }  else {
            entityManager.merge(entity);

        }
       return  entity.getId();
    }


    public void saveAll(List<T> entities) {
            for (T entity: entities) {
                entityManager.persist(entity);
            }

    }

    public void merge(T entity) {
        this.entityManager.merge(entity);
    }

    public void deleteFromDb(T entity) {
        this.entityManager.remove(this.entityManager.merge(entity));
    }

    public void delete(T entity) {
        if (entity instanceof Deletable) {
            ((Deletable) entity).setDeleted(true);
            merge(entity);
        } else {
            throw new RuntimeException("Object is not deletable, id + " + entity.getId());
        }
    }

    public void delete(Long id) {
        T entity = find(id);
        delete(entity);
    }


    public void detachEntity(T entity) {
        entityManager.detach(entity);
    }

    public T find(Long id) {
        return this.entityManager.find(entityClass, id);
    }

    public void flush() {
        entityManager.flush();
    }

    public List<T> listAll() {
        CriteriaQuery cq = this.entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return this.entityManager.createQuery(cq).getResultList();
    }

    public  List<T> list() {
        return list("");
    }

    public List<T> list(String columnName) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<T> entity = cq.from(entityClass);
        cq.select(entity);
        cq.where(cb.or(cb.isFalse(entity.get("deleted")), cb.isNull(entity.get("deleted"))));
        if (columnName.length() > 0) {
            cq.orderBy(cb.asc(entity.get(columnName)));
        }
        return this.entityManager.createQuery(cq).getResultList();
    }


}