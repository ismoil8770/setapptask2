package uz.setapp.demo.dao;



import uz.setapp.demo.entity.BaseEntity;

import java.util.List;

/**
 * The interface Abstract dao.
 *
 * @param <T> the type parameter
 */
public interface AbstractDao<T extends BaseEntity> {

    /**
     * Create.
     *
     * @param entity the entity
     */
    void create(T entity);

    /**
     * Merge.
     *
     * @param entity the entity
     */
    void merge(T entity);

    /**
     * Save long.
     *
     * @param entity the entity
     * @return the long
     */
    long save(T entity);

    void saveAll(List<T> entities);
    /**
     * Delete.
     *
     * @param entity the entity
     */
    void delete(T entity);

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(Long id);

    /**
     * Detach entity.
     *
     * @param entity the entity
     */
    void detachEntity(T entity);

    /**
     * Delete from db.
     *
     * @param entity the entity
     */
    void deleteFromDb(T entity);

    /**
     * Flush.
     */
    void flush();

    /**
     * Find t.
     *
     * @param id the id
     * @return the t
     */
    T find(Long id);

    /**
     * List list.
     *
     * @return the list
     */
    List<T> list();

    /**
     * List list.
     *
     * @param orderByColumnName the order by column name
     * @return the list
     */
    List<T> list(String orderByColumnName);

    /**
     * List all list.
     *
     * @return the list
     */
    List<T> listAll();

}
