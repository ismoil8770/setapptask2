package uz.setapp.demo.dao;


/**
 * The interface Deletable.
 */
public interface Deletable {

    /**
     * Sets deleted.
     *
     * @param d the d
     */
    void setDeleted(Boolean d);
}
